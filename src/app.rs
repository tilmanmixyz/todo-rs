use crate::command;
use lazy_static::lazy_static;
use owo_colors::OwoColorize;
use todo_lib::{error::TodoError, project::Project};

lazy_static! {
    static ref PROJECT: Project = Project::new("", "", "rodos");
}

pub fn run() {
    match todo_lib::utils::init(&PROJECT) {
        Ok(created) => {
            if created == true {
                println!(
                    "Created Directory: {dir}",
                    dir = &PROJECT.data_dir().to_str().unwrap().green().bold()
                );
                println!(
                    "Created File: {file}",
                    file = &PROJECT.data_file().to_str().unwrap().green().bold()
                );
            }
        }
        Err(err) => {
            handle_err(err);
        }
    };

    let args = command::Command::new();

    match args.command().clone().as_str() {
        "a" => add(args.argument()),
        "add" => add(args.argument()),
        "l" => list(),
        "ls" => list(),
        "list" => list(),
        "r" => remove(args.argument()),
        "rm" => remove(args.argument()),
        "remove" => remove(args.argument()),
        "d" => done(args.argument()),
        "done" => done(args.argument()),

        _ => {
            help();
        }
    }
}

fn handle_err(err: TodoError) {
    match err {
        TodoError::JsonError(_) => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Unable to parse json".italic()
            );
        }
        TodoError::FileReadError(_) => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Unable to read filein which todos are stored".italic()
            );
        }
        TodoError::NoTitleError => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "No title provided, please provide a title".italic()
            );
        }
        TodoError::IDParseError => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Could nor parse id, please provide a positive number".italic()
            );
        }
        TodoError::InvalidIDError => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Could not find inputed id, please provide a valid id".italic()
            );
        }
        TodoError::DirCreateError => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Could not create project data directory".italic()
            );
        }
        TodoError::FileCreateError => {
            println!(
                "{error}{msg}",
                error = "Error: ".red().bold(),
                msg = "Could not create project data file".italic()
            );
        }
    }

    std::process::exit(1);
}

fn done(id: String) {
    match todo_lib::todo::Todo::mark_done(id, &PROJECT) {
        Ok(_) => {
            println!("{msg}", msg = "Marked Todo as done".green().bold());
        }
        Err(e) => {
            handle_err(e);
        }
    };
}

fn add(title: String) {
    match todo_lib::todo::Todo::add(&title, &PROJECT) {
        Ok(_) => {
            println!(
                "{msg}{title}",
                msg = "Added Todo: ".green().bold(),
                title = title.as_str().italic()
            );
        }
        Err(e) => {
            handle_err(e);
        }
    };
}

fn remove(id: impl Into<String>) {
    match todo_lib::todo::Todo::remove(id, &PROJECT) {
        Ok(_) => {
            println!("{msg}", msg = "Removed Todo".green().bold());
        }
        Err(e) => {
            handle_err(e);
        }
    };
}

fn list() {
    let todos = match todo_lib::todo::Todo::get(&PROJECT) {
        Ok(todos) => todos,
        Err(e) => {
            handle_err(e);
            return;
        }
    };

    if todos.is_empty() {
        println!(
            "{error}{msg}",
            error = "Error: ".red().bold(),
            msg = "No Todos Found".italic()
        );
    }

    println!(
        "{0: <5} | {1: <20} | {2: <20} | {3: <20} | {4: <20}",
        "ID", "Title", "Created at", "Updated at", "Done"
    );

    println!();

    for todo in todos {
        println!(
            "{0: <5} | {1: <20} | {2: <20} | {3: <20} | {4: <20}",
            todo.id(),
            todo.title(),
            todo.created_at(),
            todo.modified_at(),
            if todo.done() { "Completed ?" } else { "No ?" }
        );
    }
}

fn help() {
    println!("{}", "            No command found - Showing help".bold());

    let help = format!(
        "
            {} {}
            {}
            -----

            Help:

            Command   | Arguments | Description
            {}           text        Add a new todo
            {}                       List all todos
            {}           id          Mark a todo as done
            {}           id          Delete a todo
        ",
        "Welcome to",
        "Rodos".cyan(),
        "Simple todo app written in Rust".italic(),
        "a".cyan(),
        "l".blue(),
        "d".green(),
        "r".red(),
    );

    println!("{help}");
}
