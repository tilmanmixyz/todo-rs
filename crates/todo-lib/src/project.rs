pub struct Project {
    data_dir: std::path::PathBuf,
    data_file: std::path::PathBuf,
}

impl Project {
    pub fn new(qualifier: &str, org: &str, name: &str) -> Project {
        let data_dir = (match directories::ProjectDirs::from(qualifier, org, name) {
            Some(project) => project,
            None => panic!("Could not get ProjectDirs"),
        })
        .data_dir()
        .to_owned();

        let data_file = data_dir.join("todos.json");

        Project {
            data_dir,
            data_file,
        }
    }

    /// The directory used
    pub fn data_dir(&self) -> &std::path::PathBuf {
        &self.data_dir
    }

    /// The File used
    pub fn data_file(&self) -> &std::path::PathBuf {
        &self.data_file
    }
}
