use crate::{error::TodoError, project::Project};
use serde::{Deserialize, Serialize};
use std::io::Write;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Todo {
    created_at: String,
    modified_at: String,
    done: bool,
    title: String,
    id: usize,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DataFile {
    data: Vec<Todo>,
}

impl Todo {
    fn gen_timestamp() -> String {
        chrono::Local::now().format("%d.%m, %H:%M").to_string()
    }

    fn gen_id() -> usize {
        use rand::Rng;
        let id: usize = rand::thread_rng().gen_range(1..=10000);
        id + rand::thread_rng().gen_range(1..=10000)
    }

    pub fn add(title: impl Into<String>, project: &Project) -> Result<(), TodoError> {
        let title = title.into();
        if title.is_empty() {
            return Err(TodoError::NoTitleError);
        }

        let mut todos = crate::todo::Todo::get(&project)?;
        let todo = Todo {
            title: String::from(&title),
            ..Default::default()
        };

        todos.push(todo);
        crate::todo::Todo::save(todos, &project)?;
        Ok(())
    }

    pub fn mark_done(id: impl Into<String>, project: &Project) -> Result<(), TodoError> {
        let mut todos = crate::todo::Todo::get(&project)?;

        let id = id.into();
        let id = id.parse::<usize>().map_err(|_| TodoError::IDParseError)?;

        let exists = todos.iter().any(|todo| todo.id == id);
        if !exists {
            return Err(TodoError::InvalidIDError);
        }

        for todo in &mut todos {
            if todo.id == id {
                todo.done = true;
                todo.modified_at = Todo::gen_timestamp();
            }
        }

        crate::todo::Todo::save(todos, &project)?;
        Ok(())
    }

    pub fn remove(id: impl Into<String>, project: &Project) -> Result<(), TodoError> {
        let mut todos = crate::todo::Todo::get(&project)?;

        let id = id.into();
        let id = id.parse::<usize>().map_err(|_| TodoError::IDParseError)?;

        let exists = todos.iter().any(|todo| todo.id == id);
        if !exists {
            return Err(TodoError::InvalidIDError);
        }

        todos.retain(|todo| todo.id != id);
        crate::todo::Todo::save(todos, &project)?;

        Ok(())
    }

    pub fn get(project: &Project) -> Result<Vec<crate::todo::Todo>, TodoError> {
        let data = std::fs::read_to_string(project.data_file())?;
        let todos: crate::todo::DataFile = serde_json::from_str(&data)?;

        Ok(todos.data())
    }

    pub fn save(todos: Vec<crate::todo::Todo>, project: &Project) -> Result<(), TodoError> {
        let data_file = crate::todo::DataFile::from(todos);
        let json = serde_json::to_string(&data_file)?;

        let mut file = std::fs::File::create(project.data_file())?;
        file.write_all(json.as_bytes())?;

        Ok(())
    }

    pub fn title(&self) -> String {
        self.title.clone()
    }

    pub fn created_at(&self) -> String {
        self.created_at.clone()
    }

    pub fn modified_at(&self) -> String {
        self.modified_at.clone()
    }

    pub fn done(&self) -> bool {
        self.done
    }

    pub fn id(&self) -> usize {
        self.id
    }
}

impl Default for Todo {
    fn default() -> Todo {
        Todo {
            title: "".to_string(),
            modified_at: Todo::gen_timestamp(),
            created_at: Todo::gen_timestamp(),
            done: false,
            id: Todo::gen_id(),
        }
    }
}

impl DataFile {
    // Access Functions
    pub fn data(&self) -> Vec<Todo> {
        self.data.clone()
    }

    pub fn from(data: Vec<Todo>) -> DataFile {
        DataFile { data }
    }
}
