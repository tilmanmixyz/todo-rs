#[derive(Debug, thiserror::Error)]
pub enum TodoError {
    #[error("Could not parse Json")]
    JsonError(#[from] serde_json::Error),
    #[error("Could not read from data file")]
    FileReadError(#[from] std::io::Error),
    #[error("No Title Provided")]
    NoTitleError,
    #[error("Could not parse ID")]
    IDParseError,
    #[error("Invalid ID")]
    InvalidIDError,
    #[error("Could not create project data directory")]
    DirCreateError,
    #[error("Could not create project data file")]
    FileCreateError,
}
