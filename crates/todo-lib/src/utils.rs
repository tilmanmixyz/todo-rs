use std::io::Write;

use crate::project::Project;

/// Creates file and directories used for application
pub fn init(project: &Project) -> Result<bool, crate::error::TodoError> {
    if std::fs::metadata(project.data_dir()).is_err() {
        std::fs::create_dir(project.data_dir())?;
    };

    if std::fs::metadata(project.data_file()).is_err() {
        let mut file = std::fs::File::create(project.data_file())?;
        file.write_all(b"{\"data\":[]}")?;

        return Ok(true);
    }

    Ok(false)
}
